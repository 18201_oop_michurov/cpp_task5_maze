# Maze Path Finder

Implements A* and BFS search algorithms.

### Build

```bash
    $ cmake . && make
```

### Executables

* [demo](https://gitlab.com/18201_oop_michurov/cpp_task5_maze/blob/master/description/EXECUTABLES.md#demo)

* [pathfinder](https://gitlab.com/18201_oop_michurov/cpp_task5_maze/blob/master/description/EXECUTABLES.md#pathfinder)

* [dictionary_pathfinder and dictionary_pathfinder_std](https://gitlab.com/18201_oop_michurov/cpp_task5_maze/blob/master/description/EXECUTABLES.md#dictionary_pathfinderdictionary_pathfinder_std)

