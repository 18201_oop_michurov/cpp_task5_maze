#ifndef MAZE_COMMON_HPP
#define MAZE_COMMON_HPP


#include <vector>
#include <string>
#include <sstream>


namespace surface
{
    enum Cell_t : char
    {
        Start = 'S',
        Finish = 'F',
        EmptySpace = '.',
        Wall = '#'
    };


    struct Point
    {
        int x;
        int y;
    };


    struct SurfaceMetadata_t
    {
        bool valid;

        Point inception;
        Point destination;
    };


    SurfaceMetadata_t traverseSurface(
            std::vector<std::string> const & surface);


    bool operator<(
            Point const & p1,
            Point const & p2);


    bool operator!=(
            Point const & p1,
            Point const & p2);


    bool operator==(
            Point const & p1,
            Point const & p2);


    std::ostream & operator<<(
            std::ostream & os,
            Point const & point);
}


template <>
class std::hash<surface::Point>
{
public:
    size_t operator()(surface::Point const & point) const;
};


#endif //MAZE_COMMON_HPP
