#ifndef MAZE_DISTANCE_HPP
#define MAZE_DISTANCE_HPP


#include "surface/common.hpp"


namespace surface
{
    namespace distance
    {
        template <class Surface_t>
        auto makeDistanceFunction(
                typename Surface_t::point_t const & destination,
                int width,
                int height);

        template <class Surface_t>
        auto makeDistanceFunction(
                typename Surface_t::point_t const & destination,
                int width);

        template <class Surface_t>
        auto makeDistanceFunction(
                typename Surface_t::point_t const & destination);
    }
}


#include <algorithm>


#include "surface/surface_types/planar/planar_surface.hpp"
#include "surface/surface_types/cylinder/cylinder_surface.hpp"
#include "surface/surface_types/toroid/toroid_surface.hpp"


template <class Surface_t>
inline auto surface::distance::makeDistanceFunction(
        typename Surface_t::point_t const & destination,
        int,
        int)
{
    return [](typename Surface_t::point_t const & p) -> size_t { return 0; };
}


template <class Surface_t>
inline auto surface::distance::makeDistanceFunction(
        typename Surface_t::point_t const & destination,
        int)
{
    return [](typename Surface_t::point_t const & p) -> size_t { return 0; };
}


template <class Surface_t>
inline auto surface::distance::makeDistanceFunction(
        typename Surface_t::point_t const & destination)
{
    return [](typename Surface_t::point_t const & p) -> size_t { return 0; };
}


template <>
inline auto surface::distance::makeDistanceFunction<surface::PlanarSurface>(
        typename surface::PlanarSurface::point_t const & destination)
{
    return [end=destination](typename surface::PlanarSurface::point_t const & p) -> size_t
    {
        return std::abs(p.x - end.x) + std::abs(p.y - end.y);
    };
}


template <>
inline auto surface::distance::makeDistanceFunction<surface::CylinderSurface>(
        typename surface::CylinderSurface::point_t const & destination,
        int width)
{
    return [end=destination, width](typename surface::CylinderSurface::point_t const & p) -> size_t
    {
        size_t d1 = std::abs(end.x - p.x) + std::abs(end.y - p.y);

        size_t d2 = std::abs(end.x + width - p.x) + std::abs(end.y - p.y);
        size_t d3 = std::abs(end.x - width - p.x) + std::abs(end.y - p.y);

        return std::min({d1, d2, d3});
    };
}


template <>
inline auto surface::distance::makeDistanceFunction<surface::ToroidSurface>(
        typename surface::ToroidSurface::point_t const & destination,
        int width,
        int height)
{
    return [end=destination, width, height](typename surface::ToroidSurface::point_t const & p) -> size_t
    {
        size_t d1 = std::abs(end.x - p.x) + std::abs(end.y - p.y);

        size_t d2 = std::abs(end.x + width - p.x) + std::abs(end.y - p.y);
        size_t d3 = std::abs(end.x - width - p.x) + std::abs(end.y - p.y);
        size_t d4 = std::abs(end.x - p.x) + std::abs(end.y + height - p.y);
        size_t d5 = std::abs(end.x - p.x) + std::abs(end.y - height - p.y);

        size_t d6 = std::abs(end.x + width - p.x) + std::abs(end.y + height - p.y);
        size_t d7 = std::abs(end.x - width - p.x) + std::abs(end.y - height - p.y);
        size_t d8 = std::abs(end.x - width - p.x) + std::abs(end.y + height - p.y);
        size_t d9 = std::abs(end.x + width - p.x) + std::abs(end.y - height - p.y);

        return std::min({d1, d2, d3, d4, d5, d6, d7, d8, d9});
    };
}


#include <cstring>


#include "utility/unsafe/c_string_levenstein_distance/levenstein_distance.hpp"
#include "surface/surface_types/word_space/word_space.hpp"


template <>
inline auto surface::distance::makeDistanceFunction<surface::WordSpace>(
        typename surface::WordSpace::point_t const & destination)
{
    size_t d_len = std::strlen(destination);

    return [destination, d_len](typename surface::WordSpace::point_t const & s) -> size_t
    {
        return unsafe::levenstein_distance::getLevensteinDistance(s, destination, std::strlen(s), d_len);
    };
}


#include "utility/levenstein_distance/levenstein_distance.hpp"
#include "surface/surface_types/word_space_std/word_space.hpp"


template <>
inline auto surface::distance::makeDistanceFunction<surface::WordSpaceSTD>(
        typename surface::WordSpaceSTD::point_t const & destination)
{
    return [destination](typename surface::WordSpaceSTD::point_t const & s) -> size_t
    {
        return levenstein_distance::getLevensteinDistance(s, destination);
    };
}


#endif //MAZE_DISTANCE_HPP
