#ifndef MAZE_SURFACE_INTERFACE_HPP
#define MAZE_SURFACE_INTERFACE_HPP


#include <vector>


#include "utility/cppncurses/interface.hpp"


template <class Point_t>
class ISurface
{
public:
    virtual std::vector<Point_t> getNeighbours(Point_t const & point) const = 0;
};


template <class Point_t>
class IRepresentableSurface
        :
        public IRepresentable,
        public ISurface<Point_t>
{};


#endif //MAZE_SURFACE_INTERFACE_HPP
