#ifndef MAZE_PLANAR_SURFACE_HPP
#define MAZE_PLANAR_SURFACE_HPP


#include <string>
#include <iostream>


#include "surface/interface.hpp"
#include "surface/common.hpp"


namespace surface
{
    class PlanarSurface : public IRepresentableSurface<surface::Point>
    {
    public:
        typedef Point point_t;

        PlanarSurface() = delete;

        explicit PlanarSurface(std::vector<std::string> surface);

        std::vector<Point> getNeighbours(Point const & point) const override;

        Point const & start() const;

        Point const & finish() const;

        std::vector<std::string> repr() const override
        {
            return this->surface;
        }

    private:
        SurfaceMetadata_t metadata;

        std::vector<std::string> surface;
    };
}


#endif //MAZE_PLANAR_SURFACE_HPP
