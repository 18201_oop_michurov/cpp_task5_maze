#include <stdexcept>
#include <algorithm>
#include <random>


#include "planar_surface.hpp"


using namespace surface;


PlanarSurface::PlanarSurface(
        std::vector<std::string> surface)
        :
        metadata(surface::traverseSurface(surface)),
        surface(std::move(surface))
{
    if (not this->metadata.valid)
    {
        throw std::runtime_error("Invalid surface");
    }
}


std::vector<Point> PlanarSurface::getNeighbours(
        Point const & point) const
{
    std::vector<Point> result;

    if (point.x + 1 < this->surface[0].size() and this->surface[point.y][point.x + 1] != Cell_t::Wall)
    {
        result.push_back({point.x + 1, point.y});
    }
    if (point.x - 1 > -1 and this->surface[point.y][point.x - 1] != Cell_t::Wall)
    {
        result.push_back({point.x - 1, point.y});
    }
    if (point.y - 1 > -1 and this->surface[point.y - 1][point.x] != Cell_t::Wall)
    {
        result.push_back({point.x, point.y - 1});
    }
    if (point.y + 1 < this->surface.size() and this->surface[point.y + 1][point.x] != Cell_t::Wall)
    {
        result.push_back({point.x, point.y + 1});
    }
/*
    if (point.x - 1 > -1 and point.y - 1 > -1 and this->surface[point.y - 1][point.x - 1] != Cell_t::Wall)
    {
        result.push_back({point.x - 1, point.y - 1});
    }
    if (point.x + 1 < this->surface[0].size() and point.y + 1 < this->surface.size()
            and this->surface[point.y + 1][point.x + 1] != Cell_t::Wall)
    {
        result.push_back({point.x + 1, point.y + 1});
    }
    if (point.x - 1 > -1 and point.y + 1 < this->surface.size()
            and this->surface[point.y + 1][point.x - 1] != Cell_t::Wall)
    {
        result.push_back({point.x - 1, point.y + 1});
    }
    if (point.x + 1 < this->surface[0].size() and point.y - 1 > -1
            and this->surface[point.y - 1][point.x + 1] != Cell_t::Wall)
    {
        result.push_back({point.x + 1, point.y - 1});
    }
*/
    // std::shuffle(result.begin(), result.end(), std::mt19937(std::default_random_engine()()));

    return result;
}


Point const & PlanarSurface::start() const
{
    return this->metadata.inception;
}


Point const & PlanarSurface::finish() const
{
    return this->metadata.destination;
}
