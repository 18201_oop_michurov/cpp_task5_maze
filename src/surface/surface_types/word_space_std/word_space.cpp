#include <cstring>


#include "word_space.hpp"
#include "utility/levenstein_distance/levenstein_distance.hpp"


using namespace surface;


WordSpaceSTD::WordSpaceSTD(
        Dictionary const & dict)
        :
        dict(dict)
{}


std::vector<std::string> WordSpaceSTD::getNeighbours(
        std::string const & word) const
{
    std::vector<std::string> result;

    size_t len = word.size();

    if (len > 0)
    {
        for (auto const & other_word : this->dict.getBySize(len - 1))
        {
            if (levenstein_distance::adjacent(word, other_word))
            {
                result.emplace_back(other_word);
            }
        }
    }

    for (auto const & other_word : this->dict.getBySize(len))
    {
        if (levenstein_distance::adjacent(word, other_word))
        {
            result.emplace_back(other_word);
        }
    }

    for (auto const & other_word : this->dict.getBySize(len + 1))
    {
        if (levenstein_distance::adjacent(word, other_word))
        {
            result.emplace_back(other_word);
        }
    }

    return result;
}
