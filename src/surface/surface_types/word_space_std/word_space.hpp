#ifndef MAZE_WORD_SPACE_STD_HPP
#define MAZE_WORD_SPACE_STD_HPP


#include <string>


#include "surface/interface.hpp"
#include "utility/dictionary/dictionary.hpp"


namespace surface
{
    class WordSpaceSTD : public ISurface<std::string>
    {
    public:
        typedef std::string point_t;

        explicit WordSpaceSTD(Dictionary const & dict);

        WordSpaceSTD() = delete;
        ~WordSpaceSTD() = default;

        std::vector<std::string> getNeighbours(std::string const & word) const override;

        WordSpaceSTD(WordSpaceSTD const & other) = default;
        WordSpaceSTD(WordSpaceSTD && other) = delete;

        WordSpaceSTD & operator=(WordSpaceSTD const & other) = delete;
        WordSpaceSTD & operator=(WordSpaceSTD && other) = delete;

    private:
        Dictionary const & dict;
    };
}


#endif //MAZE_WORD_SPACE_STD_HPP
