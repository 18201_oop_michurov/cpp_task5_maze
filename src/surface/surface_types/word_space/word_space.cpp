#include <cstring>


#include "word_space.hpp"
#include "utility/unsafe/c_string_levenstein_distance/levenstein_distance.hpp"


using namespace surface;


WordSpace::WordSpace(
        unsafe::Dictionary const & dict)
        :
        dict(dict)
{}


std::vector<char const *> WordSpace::getNeighbours(
        char const * const & word) const
{
    std::vector<char const *> result;

    size_t len = std::strlen(word);

    if (len > 0)
    {
        for (auto const other_word : this->dict.getBySize(len - 1))
        {
            if (unsafe::levenstein_distance::adjacent(word, other_word, len, len - 1))
            {
                result.push_back(other_word);
            }
        }
    }

    for (auto const other_word : this->dict.getBySize(len))
    {
        if (unsafe::levenstein_distance::adjacent(word, other_word, len, len))
        {
            result.push_back(other_word);
        }
    }

    for (auto const other_word : this->dict.getBySize(len + 1))
    {
        if (unsafe::levenstein_distance::adjacent(word, other_word, len, len + 1))
        {
            result.push_back(other_word);
        }
    }

    return result;
}
