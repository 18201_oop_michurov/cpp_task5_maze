#ifndef MAZE_WORD_SPACE_HPP
#define MAZE_WORD_SPACE_HPP


#include <string>


#include "surface/interface.hpp"
#include "utility/unsafe/c_string_dictionary/dictionary.hpp"


namespace surface
{
    class WordSpace : public ISurface<char const *>
    {
    public:
        typedef char const * point_t;

        explicit WordSpace(unsafe::Dictionary const & dict);

        WordSpace() = delete;
        ~WordSpace() = default;

        std::vector<char const *> getNeighbours(char const * const & word) const override;

        WordSpace(WordSpace const & other) = default;
        WordSpace(WordSpace && other) = delete;

        WordSpace & operator=(WordSpace const & other) = delete;
        WordSpace & operator=(WordSpace && other) = delete;

    private:
        unsafe::Dictionary const & dict;
    };
}


#endif //MAZE_WORD_SPACE_HPP
