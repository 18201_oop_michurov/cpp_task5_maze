#ifndef MAZE_SEARCH_ALGORITHM_HPP
#define MAZE_SEARCH_ALGORITHM_HPP


#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <queue>
#include <algorithm>
#include <chrono>
#include <thread>
#include <functional>


#include "surface/interface.hpp"
#include "utility/cppncurses/cppcurses.hpp"


template <class Point_t, class Line_t>
std::vector<Point_t> findPath(
        IRepresentableSurface<Point_t> const & surface,
        Point_t const & inception,
        Point_t const & destination,
        std::function<size_t(Point_t const &)> h);


template <class Point_t>
std::vector<Point_t> findPath(
        ISurface<Point_t> const & surface,
        Point_t const & inception,
        Point_t const & destination,
        std::function<size_t(Point_t const &)> h);


template <typename Key_t, typename Value_t>
class Map
{
public:
    explicit Map(Value_t const & default_value) : default_value(default_value)
    {}

    Value_t & operator[](Key_t const & key)
    {
        auto it = this->data.find(key);

        if (it == this->data.end())
        {
            Value_t & ref = this->data[key];
            ref = this->default_value;
            return ref;
        }

        return it->second;
    }

private:
    Value_t default_value;
    std::unordered_map<Key_t, Value_t> data;
};


template <class Point_t>
inline std::vector<Point_t> findPath(
        ISurface<Point_t> const & surface,
        Point_t const & inception,
        Point_t const & destination,
        std::function<size_t(Point_t const &)> h)
{
    auto g = Map<Point_t, size_t>(std::numeric_limits<size_t>::max() / 2);

    g[inception] = 0;

    auto f = Map<Point_t, size_t>(std::numeric_limits<size_t>::max() / 2);

    f[inception] = h(inception);

    auto comp = [&f, &h](Point_t const & p1, Point_t const & p2) -> bool
    {
        if (f[p1] == f[p2])
        {
            return h(p1) < h(p2);
        }
        return f[p1] < f[p2];
    };

    auto open = std::unordered_set<Point_t>();
    open.insert(inception);

    auto parent = std::unordered_map<Point_t, Point_t>();
    parent[inception] = inception;

    while (not open.empty())
    {
        auto c = std::min_element(open.begin(), open.end(), comp);
        Point_t current = *c;

        open.erase(c);

        if (current == destination)
        {
            std::vector<Point_t> path;

            for (Point_t vertex = destination; parent[vertex] != vertex; vertex = parent[vertex])
            {
                path.push_back(vertex);
            }

            path.push_back(inception);

            std::reverse(path.begin(), path.end());

            return path;
        }

        for (auto const & neighbour : surface.getNeighbours(current))
        {
            size_t tentative_gScore = g[current] + 1;

            if (tentative_gScore < g[neighbour])
            {
                parent[neighbour] = current;
                g[neighbour] = tentative_gScore;
                f[neighbour] = g[neighbour] + h(neighbour);

                if (std::find(open.begin(), open.end(), neighbour) == open.end())
                {
                    open.insert(neighbour);
                    // std::push_heap(open.begin(), open.end(), comp);

                    // open.insert(neighbour);
                    // open.push(neighbour);
                }
            }
        }
    }

    return {};
}

#ifdef STRING_SPECIALIZATION
template <>
inline std::vector<std::string> findPath(
        ISurface<std::string> const & surface,
        std::string const & inception,
        std::string const & destination,
        std::function<size_t(std::string const &)> h)
{
    using Point_t = std::string;
    auto g = Map<Point_t, size_t>(std::numeric_limits<size_t>::max() / 2);

    g[inception] = 0;

    auto f = Map<Point_t, size_t>(std::numeric_limits<size_t>::max() / 2);

    f[inception] = h(inception);

    auto comp = [&f, &h](Point_t const & p1, Point_t const & p2) -> bool
    {
        if (f[p1] == f[p2])
        {
            return h(p1) < h(p2);
        }
        return f[p1] < f[p2];
    };

    // auto open = std::vector<Point_t>();
    // open.push_back(inception);
    // std::make_heap(open.begin(), open.end());

    auto open = std::unordered_set<Point_t>();
    // open.insert(inception);
    open.insert(inception);

    auto parent = std::unordered_map<Point_t, Point_t>();
    parent[inception] = inception;

    while (not open.empty())
    {
        auto c = std::min_element(open.begin(), open.end(), comp);
        Point_t current = *c;
        // Point_t current = open.front();

        //std::pop_heap(open.begin(), open.end(), comp);
        // open.pop_back();

        std::cout << current << std::endl;

        open.erase(c);

        if (current == destination)
        {
            std::vector<Point_t> path;

            for (Point_t vertex = destination; parent[vertex] != vertex; vertex = parent[vertex])
            {
                path.push_back(vertex);
            }

            path.push_back(inception);

            std::reverse(path.begin(), path.end());

            return path;
        }

        for (auto const & neighbour : surface.getNeighbours(current))
        {
            size_t tentative_gScore = g[current] + 1;

            /*if (it != closed.end() and tentative_gScore >= g(neighbour))
            {
                continue;
            }*/
            if (tentative_gScore < g[neighbour])
            {
                parent[neighbour] = current;
                g[neighbour] = tentative_gScore;
                f[neighbour] = g[neighbour] + h(neighbour);

                if (std::find(open.begin(), open.end(), neighbour) == open.end())
                {
                    open.insert(neighbour);
                    // std::push_heap(open.begin(), open.end(), comp);

                    // open.insert(neighbour);
                    // open.push(neighbour);
                }
            }
        }
    }

    return {};
}


template <>
inline std::vector<char const *> findPath(
        ISurface<char const *> const & surface,
        char const * const & inception,
        char const * const & destination,
        std::function<size_t(char const * const &)> h)
{
    using Point_t = char const *;
    auto g = Map<Point_t, size_t>(std::numeric_limits<size_t>::max() / 2);

    g[inception] = 0;

    auto f = Map<Point_t, size_t>(std::numeric_limits<size_t>::max() / 2);

    f[inception] = h(inception);

    auto comp = [&f, &h](Point_t const & p1, Point_t const & p2) -> bool
    {
        if (f[p1] == f[p2])
        {
            return h(p1) < h(p2);
        }
        return f[p1] < f[p2];
    };

    // auto open = std::vector<Point_t>();
    // open.push_back(inception);
    // std::make_heap(open.begin(), open.end());

    auto open = std::unordered_set<Point_t>();
    // open.insert(inception);
    open.insert(inception);

    auto parent = std::unordered_map<Point_t, Point_t>();
    parent[inception] = inception;

    while (not open.empty())
    {
        auto c = std::min_element(open.begin(), open.end(), comp);
        Point_t current = *c;
        // Point_t current = open.front();

        //std::pop_heap(open.begin(), open.end(), comp);
        // open.pop_back();

        std::cout << current << std::endl;

        open.erase(c);

        if (current == destination)
        {
            std::vector<Point_t> path;

            for (Point_t vertex = destination; parent[vertex] != vertex; vertex = parent[vertex])
            {
                path.push_back(vertex);
            }

            path.push_back(inception);

            std::reverse(path.begin(), path.end());

            return path;
        }

        for (auto const & neighbour : surface.getNeighbours(current))
        {
            size_t tentative_gScore = g[current] + 1;

            /*if (it != closed.end() and tentative_gScore >= g(neighbour))
            {
                continue;
            }*/
            if (tentative_gScore < g[neighbour])
            {
                parent[neighbour] = current;
                g[neighbour] = tentative_gScore;
                f[neighbour] = g[neighbour] + h(neighbour);

                if (std::find(open.begin(), open.end(), neighbour) == open.end())
                {
                    open.insert(neighbour);
                    // std::push_heap(open.begin(), open.end(), comp);

                    // open.insert(neighbour);
                    // open.push(neighbour);
                }
            }
        }
    }

    return {};
}
#endif

template <class Point_t>
inline std::vector<Point_t> findPath(
        IRepresentableSurface<Point_t> const & surface,
        Point_t const & inception,
        Point_t const & destination,
        std::function<size_t(Point_t const &)> h)
{
    auto copy = std::move(surface.repr());

    std::ostringstream buff;

    auto g = Map<Point_t, size_t>(std::numeric_limits<size_t>::max() / 2);

    g[inception] = 0;

    auto f = Map<Point_t, size_t>(std::numeric_limits<size_t>::max() / 2);

    f[inception] = h(inception);

    auto comp = [&f, &h](Point_t const & p1, Point_t const & p2) -> bool
    {
        if (f[p1] == f[p2])
        {
            return h(p1) < h(p2);
        }
        return f[p1] < f[p2];
    };

    auto open = std::unordered_set<Point_t>();
    open.insert(inception);

    auto parent = std::unordered_map<Point_t, Point_t>();
    parent[inception] = inception;

    auto & terminal = Curses::getInstance();

    terminal.displayRepresentable(0, 1, surface);

    // auto max_y = Curses::getCursorPosition().y + 1;

    while (not open.empty())
    {
        auto c = std::min_element(open.begin(), open.end(), comp);
        Point_t current = *c;

        open.erase(c);

        if (not (current == inception) and not (current == destination))
        {
            terminal.putChar(current.x, current.y + 1, '*');
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(25));

        if (current == destination)
        {
            std::vector<Point_t> path;

            for (Point_t vertex = destination; parent[vertex] != vertex; vertex = parent[vertex])
            {
                path.push_back(vertex);

                if (not (vertex == inception) and not (vertex == destination))
                {
                    terminal.putChar(vertex.x, vertex.y + 1, '+');
                }

                std::this_thread::sleep_for(std::chrono::milliseconds(40));
            }

            path.push_back(inception);

            std::reverse(path.begin(), path.end());

            // terminal.setCursorPosition(0, max_y);

            return path;
        }

        for (auto const & neighbour : surface.getNeighbours(current))
        {
            size_t tentative_gScore = g[current] + 1;

            if (tentative_gScore < g[neighbour])
            {
                parent[neighbour] = current;
                g[neighbour] = tentative_gScore;
                f[neighbour] = g[neighbour] + h(neighbour);

                if (std::find(open.begin(), open.end(), neighbour) == open.end())
                {
                    open.insert(neighbour);
                }
            }
        }
    }

    // terminal.setCursorPosition(0, max_y);

    return {};
}


#endif //MAZE_SEARCH_ALGORITHM_HPP
