#include <iostream>
#include <fstream>
#include <cmath>
#include <functional>


#include "surface/surface_types/planar/planar_surface.hpp"
#include "surface/surface_types/toroid/toroid_surface.hpp"
#include "surface/surface_types/cylinder/cylinder_surface.hpp"
#include "search_algorithm/search_algorithm.hpp"
#include "surface/distance/distance.hpp"
#include "utility/arg_parser/parser.hpp"


static inline void printHelp()
{
    static char const * help =
            R"=(Usage: pathfinder [KEYS]
Finds and prints shortest possible path in a maze depending on surface topology.

Allowed keys:
    -p, --planar               set surface topology to planar

    -c, --cylinder             set surface topology to cylinder
                               (allowed to jump from left border to right
                               border and vice versa)

    -t, --tor                  set surface topology to tor
                               (allowed to jump from left border to right
                               border, form top to bottom and vice versa)

    -i, --in=IN_FILE_NAME      read surface from IN_FILE_NAME (read form stdin by default)

    -o, --out=OUT_FILE_NAME    write output to OUT_FILE_NAME (write to stdout by default)

    -h, --help                 display help (the one you're reading right now))=";
    std::cout << help << std::endl;
}


int main(
        int argc,
        char * argv[])
{
    std::vector<std::string> surface_lines;

    arg_parser::surface::GlobalArgs_t global_args;

    try
    {
        global_args = arg_parser::surface::parse_arguments(argc, argv);
    }
    catch (std::runtime_error & e)
    {
        std::cout << "Error: " << e.what() << std::endl << std::endl;

        printHelp();

        exit(EXIT_FAILURE);
    }

    if (global_args.help)
    {
        printHelp();
        exit(EXIT_SUCCESS);
    }

    std::ifstream in_file;

    if (not global_args.in_file.empty())
    {
        in_file.open(global_args.in_file);

        if (in_file.fail())
        {
            std::cout << "Что-то у меня не получилось открыть файл \"" << global_args.in_file << "\"" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    std::istream & in = in_file.is_open() ? in_file : std::cin;

    std::string buff;

    while (std::getline(in, buff))
    {
        if (not buff.empty())
        {
            surface_lines.push_back(buff);
            buff.clear();
        }
    }

    std::vector<surface::Point> path;

    std::unique_ptr<ISurface<surface::Point>> surface;
    std::function<size_t(surface::Point const &)> distance;

    int height = surface_lines.size();
    int width = surface_lines[0].size();

    surface::Point start = {-1, -1};
    surface::Point finish = {-1, -1};

    switch (global_args.topology)
    {
        case arg_parser::surface::Topology::Planar:
            try
            {
                auto surface_p = std::make_unique<surface::PlanarSurface>(surface_lines);
                start = surface_p->start();
                finish = surface_p->finish();

                distance = surface::distance::makeDistanceFunction<surface::PlanarSurface>(finish);
                surface = std::move(surface_p);
            }
            catch (std::runtime_error & e)
            {
                std::cout << "Error: " << e.what() << std::endl;
                exit(EXIT_FAILURE);
            }
            break;
        case arg_parser::surface::Topology::Cylinder:
            try
            {
                auto surface_p = std::make_unique<surface::CylinderSurface>(surface_lines);
                start = surface_p->start();
                finish = surface_p->finish();

                distance = surface::distance::makeDistanceFunction<surface::CylinderSurface>(finish, width);
                surface = std::move(surface_p);
            }
            catch (std::runtime_error & e)
            {
                std::cout << "Error: " << e.what() << std::endl;
                exit(EXIT_FAILURE);
            }
            break;
        case arg_parser::surface::Topology::Toroid:
            try
            {
                auto surface_p = std::make_unique<surface::ToroidSurface>(surface_lines);
                start = surface_p->start();
                finish = surface_p->finish();

                distance = surface::distance::makeDistanceFunction<surface::ToroidSurface>(finish, width, height);
                surface = std::move(surface_p);
            }
            catch (std::runtime_error & e)
            {
                std::cout << "Error: " << e.what() << std::endl;
                exit(EXIT_FAILURE);
            }
            break;
    }

    path = findPath(
            *surface,
            start,
            finish,
            distance
    );

    std::ofstream out_file;

    if (not global_args.out_file.empty())
    {
        out_file.open(global_args.out_file);

        if (out_file.fail())
        {
            std::cout << "Что-то у меня не получилось открыть файл \"" << global_args.out_file << "\"" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    std::ostream & out = out_file.is_open() ? out_file : std::cout;

    if (path.empty())
    {
        out << "No path" << std::endl;
    }
    else
    {
        for (auto const & point : path)
        {
            out << point << "\n";
        }

        out.flush();
    }

    exit(EXIT_SUCCESS);
}
