#ifndef MAZE_DICTIONARY_HPP
#define MAZE_DICTIONARY_HPP


#include <unordered_map>
#include <vector>


class WordsList
{
public:
    class const_iterator;

    const_iterator begin() const;

    const_iterator end() const;

    WordsList();

    explicit WordsList(size_t word_size);

    ~WordsList() = default;

    WordsList & append(std::string const & new_word);

    WordsList & operator+=(std::string const & new_word);

    std::string const & operator[](size_t index) const;

    size_t size() const;

    size_t word_size() const;

private:
    size_t m_word_size;
    std::vector<std::string> list;
};


class WordsList::const_iterator : std::iterator<std::input_iterator_tag, std::string>
{
    friend class WordsList;

    const_iterator(
            size_t index,
            std::vector<std::string> const & list);

public:
    const_iterator & operator++();

    std::string const & operator*() const;

    bool operator!=(const_iterator const & other) const;

private:
    size_t index;
    std::vector<std::string> const & list;
};


class Dictionary
{
public:
    class const_iterator;

    /*
     * Iterate over WordLists
     */

    const_iterator begin() const;

    const_iterator end() const;

    Dictionary() = default;
    ~Dictionary() = default;

    Dictionary & append(std::string const & new_word);

    Dictionary & operator+=(std::string const & new_word);

    WordsList const & getBySize(size_t word_size) const;

private:
    WordsList empty_list;
    std::unordered_map<size_t, WordsList> dict;
};


class Dictionary::const_iterator : std::iterator<std::input_iterator_tag, WordsList const>
{
    friend class Dictionary;

    explicit const_iterator(
            std::unordered_map<size_t, WordsList>::const_iterator const & it);

public:
    typedef std::input_iterator_tag iterator_category;

    const_iterator & operator++();

    WordsList const & operator*() const;

    bool operator!=(const_iterator const & other) const;

private:
    std::unordered_map<size_t, WordsList>::const_iterator it;
};


namespace std
{
    template <>
    struct iterator_traits<WordsList::const_iterator>
    {
        typedef size_t difference_type;
        typedef std::string value_type;
        typedef std::string * pointer;
        typedef std::string & reference;
        typedef std::input_iterator_tag iterator_category;
    };

    template <>
    struct iterator_traits<Dictionary::const_iterator>
    {
        typedef size_t difference_type;
        typedef WordsList value_type;
        typedef WordsList * pointer;
        typedef WordsList & reference;
        typedef std::input_iterator_tag iterator_category;
    };
}


#endif //MAZE_DICTIONARY_HPP
