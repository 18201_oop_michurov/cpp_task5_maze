#include <cstring>
#include <algorithm>


#include "dictionary.hpp"


WordsList::const_iterator::const_iterator(
        size_t index,
        std::vector<std::string> const & list)
        :
        index(index),
        list(list)
{}


WordsList::const_iterator & WordsList::const_iterator::operator++()
{
    this->index += 1;
    return *this;
}


std::string const & WordsList::const_iterator::operator*() const
{
    return this->list[this->index];
}


bool WordsList::const_iterator::operator!=(
        const_iterator const & other) const
{
    return this->index != other.index or this->list != other.list;
}


WordsList::const_iterator WordsList::begin() const
{
    return {0, this->list};
}


WordsList::const_iterator WordsList::end() const
{
    return {this->size(), this->list};
}


WordsList::WordsList()
        :
        m_word_size(0),
        list()
{}


WordsList::WordsList(
        size_t word_size)
        :
        m_word_size(word_size),
        list()
{}


WordsList & WordsList::append(
        std::string const & new_word)
{
    if (this->m_word_size == 0)
    {
        this->m_word_size = new_word.size();
    }
    else if (this->m_word_size != new_word.size())
    {
        throw std::length_error(
                "Bad word length: expected " + std::to_string(this->m_word_size)
                + ", got " + std::to_string(new_word.size())
        );
    }

    /*
    if (not this->find(new_word))
    {
        this->list.append(new_word);
        this->list.push_back(0);

        this->m_size += 1;
    }*/
    this->list.push_back(new_word);

    return *this;
}


WordsList & WordsList::operator+=(
        std::string const & new_word)
{
    return this->append(new_word);
}


std::string const & WordsList::operator[](
        size_t index) const
{
    return this->list[index];
}


size_t WordsList::size() const
{
    return this->list.size();
}


size_t WordsList::word_size() const
{
    return this->m_word_size;
}


Dictionary::const_iterator::const_iterator(
        std::unordered_map<size_t, WordsList>::const_iterator const & it)
        :
        it(it)
{}


Dictionary::const_iterator & Dictionary::const_iterator::operator++()
{
    ++this->it;

    return *this;
}


WordsList const & Dictionary::const_iterator::operator*() const
{
    return this->it->second;
}


bool Dictionary::const_iterator::operator!=(const_iterator const & other) const
{
    return this->it != other.it;
}


Dictionary & Dictionary::append(
        std::string const & new_word)
{
    this->dict[new_word.size()].append(new_word);

    return *this;
}


Dictionary & Dictionary::operator+=(
        std::string const & new_word)
{
    return this->append(new_word);
}


WordsList const & Dictionary::getBySize(
        size_t word_size) const
{
    try
    {
        return this->dict.at(word_size);
    }
    catch (...)
    {
        return this->empty_list;
    }
}


Dictionary::const_iterator Dictionary::begin() const
{
    return const_iterator{this->dict.begin()};
}


Dictionary::const_iterator Dictionary::end() const
{
    return const_iterator{this->dict.end()};
}
