#ifndef MAZE_LEVENSTEIN_DISTANCE_HPP
#define MAZE_LEVENSTEIN_DISTANCE_HPP


#include <string>


namespace levenstein_distance
{
    size_t getLevensteinDistance(
            std::string const & s1,
            std::string const & s2);


    bool adjacent(
            std::string const & s1,
            std::string const & s2);
}


#endif //MAZE_LEVENSTEIN_DISTANCE_HPP
