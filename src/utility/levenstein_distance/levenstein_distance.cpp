#include <memory>
#include <cstring>


#include "levenstein_distance.hpp"


size_t levenstein_distance::getLevensteinDistance(
        std::string const & s1,
        std::string const & s2)
{
    size_t const m = s1.size();
    size_t const n = s2.size();

    if (m == 0)
    {
        return n;
    }
    if ( n== 0)
    {
        return m;
    }

    auto costs = std::unique_ptr<size_t[]>(new size_t[n + 1]);

    for (size_t k = 0; k <= n; ++k)
    {
        costs[k] = k;
    }

    size_t i = 0;
    for (std::string::const_iterator it1 = s1.begin(); it1 != s1.end(); ++it1, ++i)
    {
        costs[0] = i + 1;
        size_t corner = i;

        size_t j = 0;
        for (std::string::const_iterator it2 = s2.begin(); it2 != s2.end(); ++it2, ++j)
        {
            size_t upper = costs[j + 1];
            if (*it1 == *it2)
            {
                costs[j + 1] = corner;
            }
            else
            {
                size_t t = upper < corner ? upper : corner;
                costs[j + 1] = (costs[j] < t ? costs[j] : t) + 1;
            }

            corner = upper;
        }
    }

    size_t result = costs[n];

    return result;
}


bool levenstein_distance::adjacent(
        std::string const & s1,
        std::string const & s2)
{
    size_t m = s1.size();
    size_t n = s2.size();

    // If difference between lengths is more than 
    // 1, then strings can't be at one distance 
    if (m > n + 1 or n > m + 1)
    {
        return false;
    }

    size_t count = 0; // Count of edits 

    size_t i = 0;
    size_t j = 0;
    while (i < m and j < n)
    {
        // If current characters don't match 
        if (s1[i] != s2[j])
        {
            if (count > 0)
            {
                return false;
            }

            // If length of one string is 
            // more, then only possible edit 
            // is to remove a character 
            if (m > n)
            {
                ++i;
            }
            else if (m< n)
            {
                ++j;
            }
            else //If lengths of both strings is same 
            {
                ++i;
                ++j;
            }

            ++count;
        }
        else // If current characters match 
        {
            i++;
            j++;
        }
    }

    // If last character is extra in any string 
    if (i < m or j < n)
    {
        ++count;
    }

    return count == 1;
}
