#include <cstring>
#include <algorithm>


#include "dictionary.hpp"


using namespace unsafe;


WordsList::const_iterator::const_iterator(
        size_t index,
        size_t offset,
        char const * list)
        :
        index(index),
        offset(offset),
        list(list)
{}


WordsList::const_iterator & WordsList::const_iterator::operator++()
{
    this->index += 1;
    return *this;
}


char const * WordsList::const_iterator::operator*() const
{
    return this->list + this->index * this->offset;
}


bool WordsList::const_iterator::operator!=(
        const_iterator const & other) const
{
    return this->index != other.index or this->list != other.list or this->offset != other.offset;
}


WordsList::const_iterator WordsList::begin() const
{
    return {0, this->m_word_size + 1, this->list.c_str()};
}


WordsList::const_iterator WordsList::end() const
{
    return {this->size(), this->m_word_size + 1, this->list.c_str()};
}


WordsList::WordsList()
        :
        m_word_size(0),
        m_size(0),
        list()
{}


WordsList::WordsList(
        size_t word_size)
        :
        m_word_size(word_size),
        m_size(0),
        list()
{}


WordsList & WordsList::append(
        std::string const & new_word)
{
    if (this->m_word_size == 0)
    {
        this->m_word_size = new_word.size();
    }
    else if (this->m_word_size != new_word.size())
    {
        throw std::length_error(
                "Bad word length: expected " + std::to_string(this->m_word_size)
                + ", got " + std::to_string(new_word.size())
        );
    }

    /*
    if (not this->find(new_word))
    {
        this->list.append(new_word);
        this->list.push_back(0);

        this->m_size += 1;
    }*/
    this->list.append(new_word);
    this->list.push_back(0);

    this->m_size += 1;

    return *this;
}


WordsList & WordsList::operator+=(
        std::string const & new_word)
{
    return this->append(new_word);
}


char const * WordsList::operator[](
        size_t index) const
{
    return this->list.c_str() + index * (this->m_word_size + 1);
}


char const * WordsList::find(std::string const & string) const
{
    if (this->m_word_size != string.size())
    {
        return nullptr;
    }

    for (auto const & word : *this)
    {
        if (std::strcmp(string.c_str(), word) == 0)
        {
            return word;
        }
    }

    return nullptr;
}


char const * WordsList::find(char const * string) const
{
    return this->find(std::string(string));
}


size_t WordsList::size() const
{
    return this->m_size;
}


size_t WordsList::word_size() const
{
    return this->m_word_size;
}


Dictionary::const_iterator::const_iterator(
        std::unordered_map<size_t, WordsList>::const_iterator const & it)
        :
        it(it)
{}


Dictionary::const_iterator & Dictionary::const_iterator::operator++()
{
    ++this->it;

    return *this;
}


WordsList const & Dictionary::const_iterator::operator*() const
{
    return this->it->second;
}


bool Dictionary::const_iterator::operator!=(const_iterator const & other) const
{
    return this->it != other.it;
}


Dictionary & Dictionary::append(
        std::string const & new_word)
{
    this->dict[new_word.size()].append(new_word);

    return *this;
}


Dictionary & Dictionary::operator+=(
        std::string const & new_word)
{
    return this->append(new_word);
}


WordsList const & Dictionary::getBySize(
        size_t word_size) const
{
    try
    {
        return this->dict.at(word_size);
    }
    catch (...)
    {
        return this->empty_list;
    }
}


char const * Dictionary::find(std::string const & string) const
{
    return this->getBySize(string.size()).find(string);
}


char const * Dictionary::find(char const * string) const
{
    return this->getBySize(std::strlen(string)).find(string);
}


Dictionary::const_iterator Dictionary::begin() const
{
    return const_iterator{this->dict.begin()};
}


Dictionary::const_iterator Dictionary::end() const
{
    return const_iterator{this->dict.end()};
}
