#ifndef MAZE_C_STRING_DICTIONARY_HPP
#define MAZE_C_STRING_DICTIONARY_HPP


#include <unordered_map>


namespace unsafe
{
    class WordsList
    {
    public:
        class const_iterator;

        const_iterator begin() const;

        const_iterator end() const;

        WordsList();

        explicit WordsList(size_t word_size);

        ~WordsList() = default;

        WordsList & append(std::string const & new_word);

        WordsList & operator+=(std::string const & new_word);

        char const * operator[](size_t index) const;

        char const * find(std::string const & string) const;

        char const * find(char const * string) const;

        size_t size() const;

        size_t word_size() const;

    private:
        size_t m_word_size;
        size_t m_size;
        std::string list;
    };


    class WordsList::const_iterator : std::iterator<std::input_iterator_tag, char const *>
    {
        friend class WordsList;

        const_iterator(
                size_t index,
                size_t offset,
                char const * list);

    public:
        const_iterator & operator++();

        char const * operator*() const;

        bool operator!=(const_iterator const & other) const;

    private:
        size_t index;
        size_t offset;
        char const * list;
    };


    class Dictionary
    {
    public:
        class const_iterator;

        /*
         * Iterate over WordsLists
         */

        const_iterator begin() const;

        const_iterator end() const;

        Dictionary() = default;
        ~Dictionary() = default;

        Dictionary & append(std::string const & new_word);

        Dictionary & operator+=(std::string const & new_word);

        WordsList const & getBySize(size_t word_size) const;

        char const * find(std::string const & string) const;

        char const * find(char const * string) const;

    private:
        WordsList empty_list;
        std::unordered_map<size_t, WordsList> dict;
    };


    class Dictionary::const_iterator : std::iterator<std::input_iterator_tag, WordsList>
    {
        friend class Dictionary;

        explicit const_iterator(
                std::unordered_map<size_t, WordsList>::const_iterator const & it);

    public:
        typedef std::input_iterator_tag iterator_category;

        const_iterator & operator++();

        WordsList const & operator*() const;

        bool operator!=(const_iterator const & other) const;

    private:
        std::unordered_map<size_t, WordsList>::const_iterator it;
    };

}


namespace std
{
    template <>
    struct iterator_traits<unsafe::WordsList::const_iterator>
    {
        typedef size_t difference_type;
        typedef char const * value_type;
        typedef char const ** pointer;
        typedef char const * & reference;
        typedef std::input_iterator_tag iterator_category;
    };

    template <>
    struct iterator_traits<unsafe::Dictionary::const_iterator>
    {
        typedef size_t difference_type;
        typedef unsafe::WordsList value_type;
        typedef unsafe::WordsList * pointer;
        typedef unsafe::WordsList & reference;
        typedef std::input_iterator_tag iterator_category;
    };
}


#endif //MAZE_C_STRING_DICTIONARY_HPP
