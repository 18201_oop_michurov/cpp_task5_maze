#include <memory>
#include <cstring>


#include "levenstein_distance.hpp"


size_t unsafe::levenstein_distance::getLevensteinDistance(
        char const * s1,
        char const * s2,
        size_t l1,
        size_t l2)
{
    size_t const m = l1;
    size_t const n = l2;

    if (m == 0)
    {
        return n;
    }
    if ( n== 0)
    {
        return m;
    }

    auto costs = std::unique_ptr<size_t[]>(new size_t[n + 1]);

    for (size_t k = 0; k <= n; ++k)
    {
        costs[k] = k;
    }

    size_t i = 0;
    for (auto it1 = s1; it1 != s1 + m; ++it1, ++i)
    {
        costs[0] = i + 1;
        size_t corner = i;

        size_t j = 0;
        for (auto it2 = s2; it2 != s2 + n; ++it2, ++j)
        {
            size_t upper = costs[j + 1];

            if (*it1 == *it2)
            {
                costs[j + 1] = corner;
            }
            else
            {
                size_t t = upper < corner ? upper : corner;
                costs[j + 1] = (costs[j] < t ? costs[j] : t) + 1;
            }

            corner = upper;
        }
    }

    size_t result = costs[n];

    return result;
}


bool unsafe::levenstein_distance::adjacent(
        char const * s1,
        char const * s2,
        size_t l1,
        size_t l2)
{
    size_t m = l1;
    size_t n = l2;

    // If difference between lengths is more than
    // 1, then strings can't be at one distance
    if (m > n + 1 or n > m + 1)
    {
        return false;
    }

    size_t count = 0; // Count of edits

    size_t i = 0;
    size_t j = 0;
    while (i < m and j < n)
    {
        // If current characters don't match
        if (s1[i] != s2[j])
        {
            if (count > 0)
            {
                return false;
            }

            // If length of one string is
            // more, then only possible edit
            // is to remove a character
            if (m > n)
            {
                ++i;
            }
            else if (m< n)
            {
                ++j;
            }
            else //If lengths of both strings is same
            {
                ++i;
                ++j;
            }

            ++count;
        }
        else // If current characters match
        {
            i++;
            j++;
        }
    }

    // If last character is extra in any string
    if (i < m or j < n)
    {
        ++count;
    }

    return count == 1;
}
