#ifndef MAZE_C_STRING_LEVENSTEIN_DISTANCE_HPP
#define MAZE_C_STRING_LEVENSTEIN_DISTANCE_HPP


namespace unsafe
{
    namespace levenstein_distance
    {
        size_t getLevensteinDistance(
                char const * s1,
                char const * s2,
                size_t l1,
                size_t l2);


        bool adjacent(
                char const * s1,
                char const * s2,
                size_t l1,
                size_t l2);
    }
}


#endif //MAZE_C_STRING_LEVENSTEIN_DISTANCE_HPP
