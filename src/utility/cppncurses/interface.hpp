#ifndef VISUALIZER_INTERFACE_HPP
#define VISUALIZER_INTERFACE_HPP


#include <vector>
#include <string>


class IRepresentable
{
public:
    virtual std::vector<std::string> repr() const = 0;
};


#endif //INTERFACE_HPP
