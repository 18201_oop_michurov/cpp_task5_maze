#include <csignal>
#include <iostream>


#include "cppcurses.hpp"


#pragma clang diagnostic push
#pragma ide diagnostic ignored "hicpp-signed-bitwise"


/* void sigWinchHandler(int signal)
{
    int width;
    int height;

    getmaxyx(stdscr, height, width);
    resizeterm(height + 1, width + 1);
    getch();
} */


Curses::Curses()
        :
        last_attr(0),
        auto_refresh(false)
{
    this->enterCursesMode();
    // signal(SIGWINCH, sigWinchHandler);
    atexit(reinterpret_cast<void (*)()>(endwin));
}


Curses::~Curses()
{
    this->exitCursesMode();
}


Curses & Curses::enterCursesMode()
{
    // system("clear");
    // system("clear");
    initscr();
    start_color();
    keypad(stdscr, TRUE);
    // move(0, 0);
    clear();

    init_pair(1, COLOR_BLACK, COLOR_BLACK);
    init_pair(2, COLOR_RED, COLOR_BLACK);
    init_pair(3, COLOR_GREEN, COLOR_BLACK);
    init_pair(4, COLOR_YELLOW, COLOR_BLACK);
    init_pair(5, COLOR_BLUE, COLOR_BLACK);
    init_pair(6, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(7, COLOR_CYAN, COLOR_BLACK);
    init_pair(8, COLOR_WHITE, COLOR_BLACK);

    return *this;
}


Curses & Curses::exitCursesMode()
{
    clear();
    // move(0, 0);
    // system("clear");
    endwin();

    return *this;
}


Curses & Curses::getInstance()
{
    static Curses visualizer;
    return visualizer;
}


Curses::Attribute Curses::combine(
        Curses::Attribute attr)
{
    return attr;
}


Curses & Curses::putChar(
        int x,
        int y,
        unsigned char ch,
        Curses::Attribute attr,
        Curses::Colour col)
{
    auto attribute = static_cast<unsigned int>(attr);
    auto colour = static_cast<unsigned int>(col);

    auto it = this->char_data.find(ch);
    if (it != this->char_data.end())
    {
        attron(COLOR_PAIR(it->second.colour));
        attroff(attribute);
        attroff(this->last_attr);
        this->last_attr = static_cast<int>(it->second.attribute);
        attron(this->last_attr);
    }
    else
    {
        attron(COLOR_PAIR(colour));
        attroff(this->last_attr);
        this->last_attr = attribute;
        attron(attribute);
    }
    mvaddch(y, x, ch);

    if (this->auto_refresh)
    {
        refresh();
    }

    return *this;
}


Curses & Curses::putString(
        int x,
        int y,
        std::string const & string,
        Curses::Attribute attr,
        Curses::Colour col)
{
    auto attribute = static_cast<unsigned int>(attr);
    auto colour = static_cast<unsigned int>(col);

    int dx = 0;

    for (auto c : string)
    {
        auto it = this->char_data.find(c);
        if (it != this->char_data.end())
        {
            attron(COLOR_PAIR(it->second.colour));
            attroff(attribute);
            attroff(this->last_attr);
            this->last_attr = static_cast<int>(it->second.attribute);
            attron(this->last_attr);
        }
        else
        {
            attron(COLOR_PAIR(colour));
            attroff(this->last_attr);
            this->last_attr = attribute;
            attron(attribute);
        }
        mvaddch(y, x + dx++, c);
    }

    if (this->auto_refresh)
    {
        refresh();
    }

    return *this;
}


Curses & Curses::displayRepresentable(
        int x,
        int y,
        IRepresentable const & representable,
        Curses::Attribute attr,
        Curses::Colour col)
{
    auto attribute = static_cast<unsigned int>(attr);
    auto colour = static_cast<unsigned int>(col);

    auto const repr = std::move(representable.repr());
    int dy = 0;

    for (auto const & line : repr)
    {
        int dx = 0;

        for (auto c : line)
        {
            auto it = this->char_data.find(c);
            if (it != this->char_data.end())
            {
                attron(COLOR_PAIR(it->second.colour));
                attroff(attribute);
                attroff(this->last_attr);
                this->last_attr = static_cast<int>(it->second.attribute);
                attron(this->last_attr);
            }
            else
            {
                attron(COLOR_PAIR(colour)); // NOLINT
                attroff(this->last_attr);
                this->last_attr = attribute;
                attron(attribute);
            }
            mvaddch(y + dy, x + dx++, c);
        }

        ++dy;
    }

    if (this->auto_refresh)
    {
        refresh();
    }

    return *this;
}


Curses & Curses::setCursorPosition(
        int x,
        int y)
{
    move(y, x);

    return *this;
}


Curses::CursorPosition Curses::getCursorPosition()
{
    int x;
    int y;
    getyx(stdscr, y, x);

    return {x, y};
}


Curses & Curses::setCursor(
        Curses::Visibility visibility)
{
    auto err = curs_set(static_cast<int>(visibility));

    if (err == ERR)
    {
        throw std::runtime_error("Unsupported state");
    }

    return *this;
}


Curses & Curses::refresh()
{
    ::refresh();

    return *this;
}


Curses & Curses::setAutoRefresh(bool flag)
{
    this->auto_refresh = flag;

    return *this;
}


Curses & Curses::setCharProperties(
        char c,
        Curses::Attribute attr,
        Curses::Colour colour)
{
    this->char_data[c] = {colour, attr};

    return *this;
}


Curses & Curses::setTerminalSize(
        int width,
        int height)
{
    int err = resize_term(height, width);

    if (err == ERR)
    {
        throw std::runtime_error("Terminal window resize error");
    }

    return *this;
}


int Curses::getKey()
{
    return getch();
}


Curses & Curses::clearLine(
        int y)
{
    auto const cp = this->getCursorPosition();

    move(y, 0);
    clrtoeol();

    move(cp.y, cp.x);

    return *this;
}


Curses & Curses::clearAll()
{
    clear();
    return *this;
}


Curses::CursorPosition Curses::getBounds()
{
    int x;
    int y;

    getmaxyx(stdscr, y, x);

    return {x, y};
}


#pragma clang diagnostic pop