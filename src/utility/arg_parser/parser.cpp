#include <getopt.h>


#include "parser.hpp"


static option long_options[] = {
        {"planar", no_argument, nullptr, 'p'},
        {"cylinder", no_argument, nullptr, 'c'},
        {"tor", no_argument, nullptr, 't'},
        {"in", required_argument, nullptr, 'i'},
        {"out", required_argument, nullptr, 'o'},
        {"help", no_argument, nullptr, 'h'},
        {nullptr, 0, nullptr, 0}
};


arg_parser::surface::GlobalArgs_t arg_parser::surface::parse_arguments(
        int argc,
        char ** argv)
{
    GlobalArgs_t global_args = {Topology::Planar, "", ""};

    char const * opt_string = "-:pcthi:o:";
    int arg;

    bool found_topology = false;

    opterr = 0;

    while ((arg = getopt_long(argc, argv, opt_string, long_options, nullptr)) != -1)
    {
        switch (arg)
        {
            case 'p':
                if (not found_topology)
                {
                    global_args.topology = Topology::Planar;
                    found_topology = true;
                }
                else
                {
                    throw std::runtime_error("Bad argument count: only one topology can be specified");
                }
                break;
            case 'c':
                if (not found_topology)
                {
                    global_args.topology = Topology::Cylinder;
                    found_topology = true;
                }
                else
                {
                    throw std::runtime_error("Bad argument count: only one topology can be specified");
                }
                break;
            case 't':
                if (not found_topology)
                {
                    global_args.topology = Topology::Toroid;
                    found_topology = true;
                }
                else
                {
                    throw std::runtime_error("Bad argument count: only one topology can be specified");
                }
                break;
            case 'i':
                global_args.in_file = optarg;
                break;
            case 'o':
                global_args.out_file = optarg;
                break;
            case 'h':
                global_args.help = true;
                return global_args;
            case ':':
                throw std::runtime_error(std::string(argv[optind - 1]) + " is missing argument");
            case '?':
                throw std::runtime_error("Unknown option");
            case 1:
                throw std::runtime_error(
                        "Non-option encountered: " + std::string(argv[optind - 1])
                        + "\n(this program takes no non-option arguments)"
                );
            default:
                throw std::runtime_error("Bad option: " + std::string(argv[optind - 1]));
        }
    }

    return global_args;
}


arg_parser::words::GlobalArgs_t arg_parser::words::parse_arguments(
        int argc,
        char ** argv)
{
    GlobalArgs_t global_args = {};

    char const * opt_string = ":hi:o:";
    int arg;

    opterr = 0;

    while ((arg = getopt_long(argc, argv, opt_string, long_options, nullptr)) != -1)
    {
        switch (arg)
        {
            case 'i':
                global_args.in_file = optarg;
                break;
            case 'o':
                global_args.out_file = optarg;
                break;
            case 'h':
                global_args.help = true;
                return global_args;
            case ':':
                throw std::runtime_error(std::string(argv[optind - 1]) + " is missing argument");
            case '?':
                throw std::runtime_error("Unknown option");
            default:
                throw std::runtime_error("Bad option: " + std::string(argv[optind - 1]));
        }
    }

    if (argv[optind] != nullptr)
    {
        global_args.starting_word = argv[optind];

        if (argv[optind + 1] != nullptr)
        {
            global_args.end_word = argv[optind + 1];

            if (argv[optind + 2] != nullptr)
            {
                throw std::runtime_error(
                        "Extra non-option arguments: " + std::string(argv[optind + 2])
                        + "\n(this program takes only two non-option arguments");
            }
        }
        else
        {
            throw std::runtime_error(
                    "Missing required non-option argument END_WORD");
        }
    }
    else
    {
        throw std::runtime_error(
                "Missing required non-option arguments STARTING_WORD, END_WORD");
    }

    return global_args;
}