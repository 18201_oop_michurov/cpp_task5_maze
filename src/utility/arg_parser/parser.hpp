#ifndef MAZE_PARSER_HPP
#define MAZE_PARSER_HPP


#include <iostream>


namespace arg_parser
{
    namespace surface
    {
        enum class Topology
        {
            Planar,
            Cylinder,
            Toroid
        };

        struct GlobalArgs_t
        {
            Topology topology;
            std::string in_file;
            std::string out_file;
            bool help;
        };

        GlobalArgs_t parse_arguments(
                int argc,
                char ** argv);
    }

    namespace words
    {
        struct GlobalArgs_t
        {
            std::string in_file;
            std::string out_file;
            std::string starting_word;
            std::string end_word;
            bool help;
        };

        GlobalArgs_t parse_arguments(
                int argc,
                char ** argv);
    }
}


#endif //MAZE_PARSER_HPP
