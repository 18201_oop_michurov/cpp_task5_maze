#include <iostream>
#include <fstream>


#include "surface/surface_types/word_space/word_space.hpp"
#include "surface/distance/distance.hpp"
// #define STRING_SPECIALIZATION
#include "search_algorithm/search_algorithm.hpp"
#include "utility/arg_parser/parser.hpp"


static inline void printHelp()
{
    static char const * help =
            R"=(Usage: dictionary_pathfinder [KEYS] {STARTING_WORD} {END_WORD}
Finds and prints shortest possible path in a maze depending on surface topology.

Allowed keys:
    -i, --in=IN_FILE_NAME      read dictionary from IN_FILE_NAME (read form stdin by default)

    -o, --out=OUT_FILE_NAME    write output to OUT_FILE_NAME (write to stdout by default)

    -h, --help                 display help (the one you're reading right now)

Non-option parameters:
    STARTING_WORD              (required) starting word in a path

    END_WORD                   (required) destination word)=";
    std::cout << help << std::endl;
}


int main(
        int argc,
        char * argv[])
{
    arg_parser::words::GlobalArgs_t global_args;

    try
    {
        global_args = arg_parser::words::parse_arguments(argc, argv);
    }
    catch (std::runtime_error & e)
    {
        std::cout << "Error: " << e.what() << std::endl << std::endl;

        printHelp();

        exit(EXIT_FAILURE);
    }

    if (global_args.help)
    {
        printHelp();
        exit(EXIT_SUCCESS);
    }

    std::ifstream in_file;

    if (not global_args.in_file.empty())
    {
        in_file.open(global_args.in_file);

        if (in_file.fail())
        {
            std::cout << "Что-то у меня не получилось открыть файл \"" << global_args.in_file << "\"" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    std::istream & in = in_file.is_open() ? in_file : std::cin;

    unsafe::Dictionary new_dict;

    // new_dict += global_args.starting_word;
    // new_dict += global_args.end_word;

    std::string buff;

    while (in >> buff)
    {
        new_dict += buff;
    }

    char const * start = new_dict.find(global_args.starting_word);
    char const * end = new_dict.find(global_args.end_word);

    std::ofstream out_file;

    if (not global_args.out_file.empty())
    {
        out_file.open(global_args.out_file);

        if (out_file.fail())
        {
            std::cout << "Что-то у меня не получилось открыть файл \"" << global_args.out_file << "\"" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    std::ostream & out = out_file.is_open() ? out_file : std::cout;

    if (start == nullptr or end == nullptr)
    {
        out << "No path" << std::endl;

        exit(EXIT_SUCCESS);
    }

    surface::WordSpace new_ws(new_dict);

    auto path = findPath<char const *>(
            new_ws,
            start,
            end,
            surface::distance::makeDistanceFunction<surface::WordSpace>(end)
    );

    if (path.empty())
    {
        out << "No path" << std::endl;
    }
    else
    {
        for (auto const & point : path)
        {
            out << point << "\n";
        }

        out.flush();
    }

    exit(EXIT_SUCCESS);
}
