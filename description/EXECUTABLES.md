#### demo

    Usage: demo [KEYS]
    Finds and prints shortest possible path in a maze depending on surface topology.
    Displays the operation of the algorithm as it searches for the destination.
    
    Allowed keys:
        -p, --planar               set surface topology to planar
    
        -c, --cylinder             set surface topology to cylinder
                                   (allowed to jump from left border to right
                                   border and vice versa)
    
        -t, --tor                  set surface topology to tor
                                   (allowed to jump from left border to right
                                   border, form top to bottom and vice versa)
    
        -i, --in=IN_FILE_NAME      read surface from IN_FILE_NAME (read form stdin by default;
                                   leave empty for default surface)
    
        -o, --out=OUT_FILE_NAME    write output to OUT_FILE_NAME (write to stdout by default)
    
        -h, --help                 display help (the one you're reading right now)

#### pathfinder

    Usage: pathfinder [KEYS]
    Finds and prints shortest possible path in a maze depending on surface topology.
    
    Allowed keys:
        -p, --planar               set surface topology to planar
    
        -c, --cylinder             set surface topology to cylinder
                                   (allowed to jump from left border to right
                                   border and vice versa)
    
        -t, --tor                  set surface topology to tor
                                   (allowed to jump from left border to right
                                   border, form top to bottom and vice versa)
    
        -i, --in=IN_FILE_NAME      read surface from IN_FILE_NAME (read form stdin by default)
    
        -o, --out=OUT_FILE_NAME    write output to OUT_FILE_NAME (write to stdout by default)
    
        -h, --help                 display help (the one you're reading right now)
        
#### dictionary_pathfinder/dictionary_pathfinder_std

*latter uses std::string instead of raw pointers and is generally slower*

    Usage: dictionary_pathfinder [KEYS] {STARTING_WORD} {END_WORD}
    Finds and prints shortest possible path in a maze depending on surface topology.
    
    Allowed keys:
        -i, --in=IN_FILE_NAME      read dictionary from IN_FILE_NAME (read form stdin by default)
    
        -o, --out=OUT_FILE_NAME    write output to OUT_FILE_NAME (write to stdout by default)
    
        -h, --help                 display help (the one you're reading right now)
    
    Non-option parameters:
        STARTING_WORD              (required) starting word in a path
    
        END_WORD                   (required) destination word