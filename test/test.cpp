#define CATCH_CONFIG_MAIN

#include "catch.hpp"


#include "../src/utility/unsafe/c_string_dictionary/dictionary.hpp"
#include "../src/utility/levenstein_distance/levenstein_distance.hpp"
#include "../src/surface/distance/distance.hpp"
#include "../src/search_algorithm/search_algorithm.hpp"


// TODO: write more tests (enough maybe?)


TEST_CASE("test unsafe::WordsList")
{
    unsafe::WordsList w_list;

    REQUIRE_NOTHROW(w_list.append("add"));
    REQUIRE_NOTHROW(w_list.append("del"));
    REQUIRE_NOTHROW(w_list.append("inc"));

    std::stringstream ss;

    for (auto i : w_list)
    {
        ss << i << std::endl;
    }

    REQUIRE(ss.str() == "add\ndel\ninc\n");

    REQUIRE_THROWS_AS(w_list.append("delegate"), std::length_error &);
}


TEST_CASE("test WordsList")
{
    WordsList w_list;

    REQUIRE_NOTHROW(w_list.append("add"));
    REQUIRE_NOTHROW(w_list.append("del"));
    REQUIRE_NOTHROW(w_list.append("inc"));

    std::stringstream ss;

    for (auto const & i : w_list)
    {
        ss << i << std::endl;
    }

    REQUIRE(ss.str() == "add\ndel\ninc\n");

    REQUIRE_THROWS_AS(w_list.append("delegate"), std::length_error &);
}


TEST_CASE("test unsafe::Dictionary")
{
    unsafe::Dictionary dict;

    dict += "add";
    dict += "del";
    dict += "sub";
    dict += "apple";
    dict += "delegate";
    dict += "cyanide";

    std::stringstream ss;

    for (auto const & word : dict.getBySize(3))
    {
        ss << word << std::endl;
    }

    for (auto const & word : dict.getBySize(5))
    {
        ss << word << std::endl;
    }

    for (auto const & word : dict.getBySize(7))
    {
        ss << word << std::endl;
    }

    for (auto const & word : dict.getBySize(8))
    {
        ss << word << std::endl;
    }

    REQUIRE(ss.str() == "add\ndel\nsub\napple\ncyanide\ndelegate\n");
}


TEST_CASE("test Dictionary")
{
    unsafe::Dictionary dict;

    dict += "add";
    dict += "del";
    dict += "sub";
    dict += "apple";
    dict += "delegate";
    dict += "cyanide";

    std::stringstream ss;

    for (auto const & word : dict.getBySize(3))
    {
        ss << word << std::endl;
    }

    for (auto const & word : dict.getBySize(5))
    {
        ss << word << std::endl;
    }

    for (auto const & word : dict.getBySize(7))
    {
        ss << word << std::endl;
    }

    for (auto const & word : dict.getBySize(8))
    {
        ss << word << std::endl;
    }

    REQUIRE(ss.str() == "add\ndel\nsub\napple\ncyanide\ndelegate\n");
}


TEST_CASE("test LevensteinDistance function")
{
    SECTION("relatively safe")
    {
        auto lev_distance = surface::distance::makeDistanceFunction<surface::WordSpaceSTD>("abcde");

        REQUIRE(lev_distance("aaaaa") == 4);
        REQUIRE(lev_distance("eftjp") == 5);
        REQUIRE(lev_distance("abc") == 2);
        REQUIRE(lev_distance("abcdefg") == 2);

        REQUIRE(levenstein_distance::adjacent("class", "glass"));
        REQUIRE(not levenstein_distance::adjacent("class", "gloss"));
    }
    SECTION("unsafe")
    {
        char const * s = "abcde";
        
        auto lev_distance = surface::distance::makeDistanceFunction<surface::WordSpace>(s);

        REQUIRE(lev_distance("aaaaa") == 4);
        REQUIRE(lev_distance("eftjp") == 5);
        REQUIRE(lev_distance("abc") == 2);
        REQUIRE(lev_distance("abcdefg") == 2);

        REQUIRE(unsafe::levenstein_distance::adjacent("class", "glass", 5u, 5u));
        REQUIRE(not unsafe::levenstein_distance::adjacent("class", "gloss", 5u, 5u));
    }
}


TEST_CASE("test WordSpace")
{
    unsafe::Dictionary dict;

    dict += "first";
    dict += "fist";
    dict += "fast";
    dict += "last";
    dict += "lost";
    dict += "loss";
    dict += "lose";
    dict += "lost";

    surface::WordSpace new_ws(dict);

    char const * start = dict.find("first");
    char const * end = dict.find("loss");

    REQUIRE(start != nullptr);
    REQUIRE(end != nullptr);

    auto path = findPath<char const *>(
            new_ws,
            start,
            end,
            surface::distance::makeDistanceFunction<surface::WordSpace>(end)
    );

    REQUIRE(not path.empty());
    REQUIRE(path.size() <= 6);
    REQUIRE(path.front() == start);
    REQUIRE(path.back() == end);

    std::stringstream ss;

    for (auto const node : path)
    {
        ss << node << std::endl;
    }

    REQUIRE(ss.str() == "first\nfist\nfast\nlast\nlost\nloss\n");
}


TEST_CASE("test WordSpaceSTD")
{
    Dictionary dict;

    dict += "first";
    dict += "fist";
    dict += "fast";
    dict += "last";
    dict += "lost";
    dict += "loss";
    dict += "lose";
    dict += "lost";

    surface::WordSpaceSTD new_ws(dict);

    auto path = findPath<std::string>(
            new_ws,
            "first",
            "loss",
            surface::distance::makeDistanceFunction<surface::WordSpaceSTD>("loss")
    );

    REQUIRE(not path.empty());
    REQUIRE(path.size() <= 6);
    REQUIRE(path.front() == "first");
    REQUIRE(path.back() == "loss");

    std::stringstream ss;

    for (auto const & node : path)
    {
        ss << node << std::endl;
    }

    REQUIRE(ss.str() == "first\nfist\nfast\nlast\nlost\nloss\n");
}
